﻿using System.IO;
using System.Linq;

namespace Directory
{
    public class DirectoryHelper
    {
        private string _path { get; set; }
        public DirectoryHelper(string path)
        {
            _path = path;
        }

        /// <summary>
        ///     Validate the input path and return boolean
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static bool IsValidPath(string path)
        {
            if (path.IndexOfAny(Path.GetInvalidPathChars()) == -1)
            {
                if (Path.IsPathRooted(path))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     return length of root path for a given path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static int GetRootPathLength(string path)
        {
            var rootPath = Path.GetPathRoot(path);
            return rootPath.Length;
        }
        /// <summary>
        ///     Extract the directory and file name from a given path
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public void FetchFileNameAndDirectoryFromPath(out string directory,out string file)
        {
            directory = null;
            file = null ;
            if (!string.IsNullOrEmpty(_path) && !IsValidPath(_path)) return;
            var rootPathLength = GetRootPathLength(_path);
            var pathLength = _path.Length;
            
            if (pathLength > rootPathLength && Path.EndsInDirectorySeparator(_path))
            {
                pathLength--;
            }

            for (var pivot = pathLength - 1; pivot >= rootPathLength; pivot--)
            {
                if (string.Equals(_path[pivot], Path.DirectorySeparatorChar))
                {
                    directory = _path.Substring(0, pivot);
                    file = _path.Substring(pivot + 1, pathLength - pivot - 1);
                    return;
                }             
            }
            directory = _path;
            return;
        }
    }
}
