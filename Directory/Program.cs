﻿using System;

namespace Directory
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryHelper obj = new DirectoryHelper(@"C:\Documents\_Assignment\Assignment_06112019MP.pdf");
            obj.FetchFileNameAndDirectoryFromPath(out var directory,out var file);
            Console.WriteLine($"Directory is - {directory}");
            Console.WriteLine($"File Name is - {file}");
        }
    }
}
